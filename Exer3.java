import java.util.Scanner;
public class Exer3 {
    public static void main(String[] args) {
        Scanner entrada;
        float SalarioHora,Pagar,PagoHoraExtra,Cincuenta,Total;
        int Horas,HorasExtra;
        entrada = new Scanner(System.in);
        System.out.println("Ingrese el total de horas laboradas: ");
        Horas = entrada.nextInt();
        System.out.println("Ingrese el salario por hora: ");
        SalarioHora = entrada.nextFloat();
        if (Horas < 38)
        {
            Pagar = Horas * SalarioHora;
            if (Pagar <= 750)
            {
                System.out.println("0% de intereses, salario de: " + Pagar);
            }
            else
            {
                System.out.println("10% de intereses, salario de: " + (0.9 *Pagar));
            }
        }
        else
        {
            Pagar = Horas * SalarioHora;
            HorasExtra = Horas - 37;
            Cincuenta = SalarioHora/2;
            PagoHoraExtra = HorasExtra * Cincuenta;
            Total = PagoHoraExtra + Pagar;
            if (Total <= 750)
            {
                System.out.println("0% de intereses, salario de: " + Total);
            }
            else
            {
                System.out.println("10% de intereses, salario de: " + (0.9 * Total));
            }
        }
    }
}