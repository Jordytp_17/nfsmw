import java.util.Scanner;
public class EMatrix3 {
    public static void main(String[] args) {
        Scanner entrada;
        float matriz[][] = new float[3][3];
        float p1=0,p2=0,p3=0,s1=0,s2=0,s3=0;
        float principal=0,secundaria=0;
        entrada = new Scanner(System.in);
        for(int i=0;i<matriz.length;i++)
        {
            for(int j=0;j<matriz[i].length;j++)
            {
            System.out.print("Ingrese el elemento["+i+"]["+j+"]: ");
            matriz[i][j] = entrada.nextFloat();
            }
        }
        for(int i=0;i<matriz.length;i++)
        {
            for(int j=0;j<matriz[i].length;j++)
            {
            System.out.print(matriz[i][j] + "\t");
            }
            System.out.println();
        }
        p1 = ((matriz[0][0])*(matriz[1][1])*(matriz[2][2]));
        p2 = ((matriz[1][0])*(matriz[2][1])*(matriz[0][2]));
        p3 = ((matriz[2][0])*(matriz[0][1])*(matriz[1][2]));
        principal = (p1 + p2 + p3);
        s1 = ((matriz[0][2])*(matriz[1][1])*(matriz[2][0]));
        s2 = ((matriz[1][2])*(matriz[2][1])*(matriz[0][0]));
        s3 = ((matriz[0][1])*(matriz[1][0])*(matriz[2][2]));
        secundaria = (s1 + s2 + s3);
        System.out.println("El determinante es: " + (principal - secundaria));
    }
}