/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Departamento;

/**
 *
 * @author Sistemas26
 */
public class Docente extends DepartamentoDocente{
    private int id;
    private int NoDeTrabajador;
    private String Nombres;
    private String Apellidos;
    private int telefono;
    private String Categoria;
    private String levelAcademic;
    private String DepartamentoDoce;

    public Docente(int id, int NoDeTrabajador, String Nombres, String Apellidos, int telefono, String Categoria, String levelAcademic, String DepartamentoDoce) {
        this.id = id;
        this.NoDeTrabajador = NoDeTrabajador;
        this.Nombres = Nombres;
        this.Apellidos = Apellidos;
        this.telefono = telefono;
        this.Categoria = Categoria;
        this.levelAcademic = levelAcademic;
        this.DepartamentoDoce = DepartamentoDoce;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNoDeTrabajador() {
        return NoDeTrabajador;
    }

    public void setNoDeTrabajador(int NoDeTrabajador) {
        this.NoDeTrabajador = NoDeTrabajador;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String Categoria) {
        this.Categoria = Categoria;
    }

    public String getLevelAcademic() {
        return levelAcademic;
    }

    public void setLevelAcademic(String levelAcademic) {
        this.levelAcademic = levelAcademic;
    }

    public String getDepartamentoDoce() {
        return DepartamentoDoce;
    }

    public void setDepartamentoDoce(String DepartamentoDoce) {
        this.DepartamentoDoce = DepartamentoDoce;
    }

    public Docente() {
    }
    
}
