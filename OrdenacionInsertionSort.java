public class OrdenacionInsertionSort{
    public static void main(String[] args) {
        
        int edades[] = {25,10,17,15,5,7,20,30,40,35,47,1};

        System.out.println("Arreglo sin ordenar: ");
        System.out.println(java.util.Arrays.toString(edades));
        System.out.println("==================================");
        insertionSortImperative(edades);
        System.out.println("Arreglo ordenado: ");
        System.out.println("==================================");
        System.out.println(java.util.Arrays.toString(edades));

    }
    public static void insertionSortImperative(int[] input) {
        for (int i = 1; i < input.length; i++) { 
            int key = input[i]; 
            int j = i - 1;
            while (j >= 0 && input[j] > key) {
                input[j + 1] = input[j];
                j = j - 1;
            }
            input[j + 1] = key; 
        }
    }

}