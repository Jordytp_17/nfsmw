/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

/**
 *
 * @author Sistemas36
 */
public class Registro {
    private int Id;
    private String Nombres;
    private String Apellidos;
    private String NombreEmpresa;
    private byte Natural;
    private int Telefono;
    private String Cedula;
    private String Correo;
    private String Direccion;

    public Registro() {
    }

    public Registro(int Id, String Nombres, String Apellidos, String NombreEmpresa, byte Natural, int Telefono, String Cedula, String Correo, String Direccion) {
        this.Id = Id;
        this.Nombres = Nombres;
        this.Apellidos = Apellidos;
        this.NombreEmpresa = NombreEmpresa;
        this.Natural = Natural;
        this.Telefono = Telefono;
        this.Cedula = Cedula;
        this.Correo = Correo;
        this.Direccion = Direccion;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getNombreEmpresa() {
        return NombreEmpresa;
    }

    public void setNombreEmpresa(String NombreEmpresa) {
        this.NombreEmpresa = NombreEmpresa;
    }

    public byte getNatural() {
        return Natural;
    }

    public void setNatural(byte Natural) {
        this.Natural = Natural;
    }

    public int getTelefono() {
        return Telefono;
    }

    public void setTelefono(int Telefono) {
        this.Telefono = Telefono;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    @Override
    public String toString() {
        return "Registro{" + "Id=" + Id + ", Nombres=" + Nombres + ", Apellidos=" + Apellidos + ", NombreEmpresa=" + NombreEmpresa + ", Natural=" + Natural + ", Telefono=" + Telefono + ", Cedula=" + Cedula + ", Correo=" + Correo + ", Direccion=" + Direccion + '}';
    }
    
    
}   
    