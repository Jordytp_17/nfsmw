import java.util.Arrays;
public class BinarySearch{
    public static void main(String[] args) {
        
        int edades[] = {33,25,11,8,15,22,7,14,35,1,17,19,19,45};
        int pos = -1, value = 15;
        Arrays.sort(edades);
        System.out.println(Arrays.toString(edades));
        pos = runBinarySearchIteratively(edades, value, 0, edades.length -1);
        System.out.format("%20s %20s\n","Valor","posicion");
        //System.out.format("El valor %t Posicion");
        if(pos == -1)
        {
            System.out.format("%20d %20d\n",value,pos);
            //System.out.println(value +  "%t" + pos);
            return;
        }
        System.out.format("%20d %20d\n",value,(pos+1));
        //System.out.format("El valor %d se encuentra en la posicion %d en la lista \n",value,(pos+1));
    }

    public static int runBinarySearchIteratively(int[] sortedArray, int key, int low, int high)
    {
        int index = Integer.MAX_VALUE;
     
        while (low <= high) {
            int mid = (low + high) / 2;
            if (sortedArray[mid] < key) {
                low = mid + 1;
            } else if (sortedArray[mid] > key) {
                high = mid - 1;
            } else if (sortedArray[mid] == key) {
                index = mid;
                break;
            }
        }
    return index;
    }

    public int runBinarySearchRecursively(
        int[] sortedArray, int key, int low, int high) {
            int middle = (low + high) / 2;
         
        if (high < low) {
            return -1;
        }
 
        if (key == sortedArray[middle]) {
            return middle;
        } else if (key < sortedArray[middle]) {
            return runBinarySearchRecursively(
            sortedArray, key, low, middle - 1);
        } else {
            return runBinarySearchRecursively(
            sortedArray, key, middle + 1, high);
        }
    }
}