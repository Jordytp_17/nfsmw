public class OrdenacionQuickSort{
    public static void main(String[] args) {
        
        int edades[] = {25,10,17,15,5,7,20,30,40,35,47,1};

        System.out.println("Arreglo sin ordenar: ");
        System.out.println(java.util.Arrays.toString(edades));
        System.out.println("==================================");
        quickSort(edades,0,edades.length-1);
        System.out.println("Arreglo ordenado: ");
        System.out.println("==================================");
        System.out.println(java.util.Arrays.toString(edades));

    }//final del main principal

    public static void quickSort(int arr[], int begin, int end) {
    if (begin < end) {
        int partitionIndex = partition(arr, begin, end);
 
        quickSort(arr, begin, partitionIndex-1);
        quickSort(arr, partitionIndex+1, end);
    }
}//final del static void quicksort

private static int partition(int arr[], int begin, int end) {
    int pivot = arr[end];
    int i = (begin-1);
 
    for (int j = begin; j < end; j++) {
        if (arr[j] <= pivot) {
            i++;
 
            int swapTemp = arr[i];
            arr[i] = arr[j];
            arr[j] = swapTemp;
        }
    }
 
    int swapTemp = arr[i+1];
    arr[i+1] = arr[end];
    arr[end] = swapTemp;
 
    return i+1;
}//final de static void partition

}//final de la clase