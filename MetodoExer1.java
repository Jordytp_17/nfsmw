import java.util.Scanner;
import java.util.Arrays;

public class MetodoExer1{
    public static void main(String[] args) {
        Scanner entrada;
        entrada = new Scanner(System.in);
        int fila=0,columna=0;
        System.out.println("Ingrese el total de filas: ");
        fila = entrada.nextInt();
        System.out.println("Ingrese el total de columnas: ");
        columna = entrada.nextInt();
        float matriz[][] = new float[fila][columna];
        float colsum[][] = new float[1][columna];
        for(int i=0;i<matriz.length;i++)
        {
            for(int j=0;j<matriz[i].length;j++)
            {
            System.out.print("Ingrese el elemento["+i+"]["+j+"]: ");
            matriz[i][j] = entrada.nextFloat();
            }
        }
        System.out.println("\n\n");
        printTable(matriz);
        float filsum[] = new float[fila];
        float suma=0;
        for(int i=0;i<matriz.length;i++)
        {
            suma=0;
            for(int j=0;j<matriz[i].length;j++)
            {
                suma = suma + matriz[i][j];
            }
            filsum[i] = suma;
        }
        System.out.println("\n\nLa suma de las filas es: \n\n");
        printArray(filsum);


    }
    public static void printArray(float array[]){
        System.out.println(Arrays.toString(array));

    }
    public static void printTable(float matriz[][]){
        for (int i=0;i<matriz.length;i++){
            printArray(matriz[i]);
        }
    }
    
    

}