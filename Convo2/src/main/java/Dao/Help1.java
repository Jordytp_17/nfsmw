/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.io.IOException;
import java.util.List;

/**
 *
 * @author Sistemas36
 */
public interface Help1 <T> {
    void create(T t) throws IOException;
    void update(T t, int id) throws IOException;
    List<T> showAll() throws IOException;
}
