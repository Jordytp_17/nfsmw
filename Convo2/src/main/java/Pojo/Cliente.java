/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pojo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Sistemas36
 */
public class Cliente {
    private int idd;
    private String nome;
    private String apell;
    private String nombreEmpresa;
    private int cellphon;
    private String cedula;
    private String correo;
    private String dire;
    private boolean active;
    private int fechaCre;
    
    public static void main(String[] args) {
    FileOutputStream fos = null;
    String text = null;
    Scanner scan = new Scanner(System.in);
    System.out.println("Escriba una linea de texto:");
    text = scan.nextLine();
    try{
    fos = new FileOutputStream("Hola.txt",true);
    fos.write(text.getBytes());
    System.out.println("El texto se escribio correctamente!");
    }catch(IOException ex){
    }finally{
    try{
    fos.close();
    }catch(IOException ex){

    }
    }
    }


    public Cliente() {
    }

    public Cliente(int idd, String nome, String apell, String nombreEmpresa, int cellphon, String cedula, String correo, String dire, boolean active, int fechaCre) {
        this.idd = idd;
        this.nome = nome;
        this.apell = apell;
        this.nombreEmpresa = nombreEmpresa;
        this.cellphon = cellphon;
        this.cedula = cedula;
        this.correo = correo;
        this.dire = dire;
        this.active = active;
        this.fechaCre = fechaCre;
    }

    public Cliente(int idd, String nome, String apell, String nombreEmpresa, int cellphon, String correo, String dire, boolean active, String fechaCre) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIdd() {
        return idd;
    }

    public void setIdd(int idd) {
        this.idd = idd;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApell() {
        return apell;
    }

    public void setApell(String apell) {
        this.apell = apell;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public int getCellphon() {
        return cellphon;
    }

    public void setCellphon(int cellphon) {
        this.cellphon = cellphon;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDire() {
        return dire;
    }

    public void setDire(String dire) {
        this.dire = dire;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getFechaCre() {
        return fechaCre;
    }

    public void setFechaCre(int fechaCre) {
        this.fechaCre = fechaCre;
    }

    @Override
    public String toString() {
        return "Cliente{" + "idd=" + idd + ", nome=" + nome + ", apell=" + apell + ", nombreEmpresa=" + nombreEmpresa + ", cellphon=" + cellphon + ", cedula=" + cedula + ", correo=" + correo + ", dire=" + dire + ", active=" + active + ", fechaCre=" + fechaCre + '}';
    }
    
       
}
