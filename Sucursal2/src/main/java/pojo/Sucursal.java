/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author Sistemas26
 */
public class Sucursal {
    public int id;                      //4
    public String nombreSucursal;       //30*2= 60+3 = 63
    public String representante;        //20*2= 40+3 = 43
    public String municipio;            // 23
    public String direccion;            //103
    public int telefono;                //4
    public String correo;               //23
    public boolean activo;              //1
    public String fechaCreacion;        //18
    public String fechaModificacion;    //18
                                        //TOTAL DE = 300

    public Sucursal() {
    }

    public Sucursal(int id, String nombreSucursal, String representante, String municipio, String direccion, int telefono, String correo, boolean activo, String fechaCreacion, String fechaModificacion) {
        this.id = id;
        this.nombreSucursal = nombreSucursal;
        this.representante = representante;
        this.municipio = municipio;
        this.direccion = direccion;
        this.telefono = telefono;
        this.correo = correo;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Override
    public String toString() {
        return "Sucursal{" + "id=" + id + ", nombreSucursal=" + nombreSucursal + ", representante=" + representante + ", municipio=" + municipio + ", direccion=" + direccion + ", telefono=" + telefono + ", correo=" + correo + ", activo=" + activo + ", fechaCreacion=" + fechaCreacion + ", fechaModificacion=" + fechaModificacion + '}';
    }
    
    
    
}
