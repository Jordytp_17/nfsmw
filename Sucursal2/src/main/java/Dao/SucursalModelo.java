/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.io.IOException;
import java.util.List;
import pojo.Sucursal;

/**
 *
 * @author Sistemas26
 */
public interface SucursalModelo extends Modelo<Sucursal>{
    Sucursal showById(int code) throws IOException;
    List<Sucursal> showByActivo(boolean activo) throws IOException;
    List<Sucursal> showByNombreSucursal(String lastName) throws IOException;
}
