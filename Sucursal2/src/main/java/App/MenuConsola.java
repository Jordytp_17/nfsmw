/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import DaoImpl.SucursalImpl;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import pojo.Sucursal;

/**
 *
 * @author Sistemas 36
 */
public class MenuConsola {
    Scanner imput = new Scanner(System.in);
    String g;
    
    public void MainMenu() throws IOException {
        SucursalImpl MOCK_DATA = new SucursalImpl();
        System.out.println("Bienvenido! - Gestor de Sucursales ");
        System.out.println("Seleccione una opcion:");
        //System.out.println("1. Registar Sucursal");
        System.out.println("1. Visualizar Todos");
        System.out.println("2. Visualizar por id");
        System.out.println("3. Salir");
        System.out.print("\n\t--> ");
        g= imput.next();
        
        try{
            int opc = Integer.parseInt(g.trim());
            switch(opc){
                /*case 1:
                    int id, telefono;
                    boolean activo;
                    String nombreSucursal,representante,municipio,direccion,correo,fechaCreacion
                        ,fechaModificacion;
                    System.out.print("ID: ");
                    id = imput.nextInt();
                    System.out.println("\nNombre de la Sucursal: ");
                    nombreSucursal = imput.next();
                    System.out.println("\nRepresentante: ");
                    representante = imput.next();
                    System.out.println("\nMunicipio: ");
                    municipio = imput.next();
                    System.out.println("\nDireccion: ");
                    direccion = imput.next();
                    System.out.println("\nTelefono: ");
                    telefono = imput.nextInt();
                    System.out.println("\nCorreo Electronico: ");
                    correo = imput.next();
                    System.out.println("\nLa sucursal esta Activa o Pasiva?");
                    activo = imput.nextBoolean();
                    System.out.println("\nFecha de creacion: ");
                    fechaCreacion = imput.next();
                    System.out.println("\nFecha de Modificacion: ");
                    fechaModificacion = imput.next();
                    Sucursal c = new Sucursal(id,nombreSucursal,representante,municipio,direccion,telefono,correo,activo,fechaCreacion
                        ,fechaModificacion);
                    MOCK_DATA.create(c);
                    System.out.println("Registro Creado!");
                    System.out.println("Presione ENTER para continuar");
                    System.in.read();
                    break;*/
                case 1:
                    List<Sucursal> sucursal = MOCK_DATA.showAll();
                    for(Sucursal t : sucursal){
                        System.out.println(t.toString());
                    }
                    System.out.println("Presione ENTER para continuar");
                    System.in.read();
                    break;
                case 2:
                    System.out.print("Ingresa el id a buscar: ");
                    int code = imput.nextInt();
                    Sucursal t = MOCK_DATA.showById(code);
                    if(t == null){
                        System.out.print("Id no Registrado");
                        System.out.println("Presione ENTER para continuar");
                        System.in.read();
                    }
                    else{
                        System.out.println(t.toString());
                        System.out.println("Presione ENTER para continuar");
                        System.in.read();
                    }
                    break;
                    
                case 3:
                    System.out.println("Tenga un buen Dia, Adios!! :)");
                    System.out.println("Presione ENTER para continuar");
                    System.in.read();
            }
        }
        catch(NumberFormatException nfe){
            System.out.println("Input Incorrecto");
            System.out.println("Presione ENTER para continuar");
            System.in.read();
            MainMenu();
        } 
    }
}
