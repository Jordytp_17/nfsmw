/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DaoImpl;

import Dao.SucursalModelo;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import pojo.Sucursal;

/**
 *
 * @author Sistemas26
 */
public class SucursalImpl implements SucursalModelo{
    private File fHead;
    private File fData;
    private RandomAccessFile rafHead;
    private RandomAccessFile rafData;
    private final int SIZE = 310; 
    private Gson gson;

    public SucursalImpl() {
        gson = new Gson();
    }
    private void open() throws FileNotFoundException, IOException{
        /*fHead = new File("MOCK_DATA");
        fData = new File("MOCK_DATA");*/
        
        rafHead = new RandomAccessFile(fHead, "rw");
        rafData = new RandomAccessFile(fData, "rw");
        
        if(rafHead.length() <= 0){
            rafHead.seek(0);
            rafHead.writeInt(0);
            rafHead.writeInt(0);
        }
    }
    
    private void close() throws IOException{
        if(rafHead != null){
            rafHead.close();
        }
        if(rafData != null){
            rafData.close();
        }
    }

    @Override
    public Sucursal showById(int id) throws IOException {
        open(); 
        rafHead.seek(0); //Vamos al inicio de la cabecera
        int n = rafHead.readInt(); //Leemos N
        int k = rafHead.readInt(); //Leemos K
        
        if(id >  n  || id <= 0){ 
            return null;
        }
        
        int index = IOCollections.binarySearchInt(rafHead, id, 0, n-1); 
        long posHead = 8 + 4 * index; 
        rafHead.seek(posHead); 
        int code = rafHead.readInt(); 
        
        long posData =(id - 1) * SIZE;
        rafData.seek(posData); 
        
        String jsonSucursal = rafData.readUTF(); 
        Sucursal e = gson.fromJson(jsonSucursal, Sucursal.class); 
        
        if(code != id){ 
            close();
            return null;
        }
        else{
            close(); 
            return e;
        }
    }

    @Override
    public List<Sucursal> showByActivo(boolean activo) throws IOException {
        List<Sucursal> sucursales = showAll();
        List<Sucursal> sucursalesName = new ArrayList<>();
        
        for(Sucursal e: sucursales){
           if(activo = true){
               sucursalesName.add(e);
           }
        }
        if(sucursalesName == null){
            return null;
        }
        else{
            return sucursalesName;
        }
    }

    @Override
    public List<Sucursal> showByNombreSucursal(String nombreSucursal) throws IOException {
        List<Sucursal> sucursales = showAll();
        List<Sucursal> sucursalesName = new ArrayList<>();
        
        for(Sucursal e: sucursales){
            if(e.equals(e)){
                sucursalesName.add(e);
            }
        }
        if(sucursalesName == null){
            return null;
        }
        else{
            return sucursalesName;
        }
    }

    @Override//NO NECESARIO
    public void create(Sucursal t) throws IOException {
        open();
        rafHead.seek(0);
        int n = rafHead.readInt(); 
        int k = rafHead.readInt(); 
        
        long posData = k * SIZE; 
        
        t.setId(k+1); 
        String jsonSucursal = gson.toJson(t); 
        
        
        rafData.seek(posData); 
        rafData.writeUTF(jsonSucursal); 
        
        //Actulizamos HEAD
        long posHead = 8 + 4 * n; 
        
        rafHead.seek(0); 
        rafHead.writeInt(++n); 
        rafHead.writeInt(++k); 
        
        rafHead.seek(posHead); 
        rafHead.writeInt(k); 

    }

    @Override//NO ES NECESARIO
    public void update(Sucursal e, int id) throws IOException {
        open(); 
        long posHead = 8 + 4 * (id - 1); 
        rafHead.seek(posHead); 
        long posData = (id - 1) * SIZE;
        e.setId(id); 
        rafData.seek(posData); 
        String jsonEmpleado = gson.toJson(e); 
        rafData.writeUTF(jsonEmpleado); 
        close(); 
    }

    @Override
    public List<Sucursal> showAll() throws IOException {
        open(); 
        List<Sucursal> sucursal = new ArrayList<>(); 
        int n = rafHead.readInt();
        rafHead.seek(0); //Nos dirigimos al inicio de la cabecera
        

        for (int i = 0; i < n; i++) { 
            long posHead = 8 + 4 * i; 
            rafHead.seek(posHead); 

            int index = rafHead.readInt();
            long posData = (index - 1)  * SIZE; 
            rafData.seek(posData); 

            String jsonCliente = rafData.readUTF(); 
            Sucursal t = gson.fromJson(jsonCliente, Sucursal.class); 
            sucursal.add(t); 
        }

        close();//SE CIERRA
        return sucursal;
    }

    
}
