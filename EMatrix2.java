import java.util.Scanner;
public class EMatrix2 {
    public static void main(String[] args) {
        Scanner entrada;
        int matriz[][];
        int orden=0;
        int suma=0;
        entrada = new Scanner(System.in);
        System.out.println("Ingrese el orden de la matriz: ");
        System.out.println("Recuerde que es de orden cuadrada, ejemplo: (2x2)");
        orden = entrada.nextInt();
        System.out.println("El orden es de: " + orden + "X" + orden);
        matriz = new int [orden][orden];
        for(int i=0;i<matriz.length;i++)
        {
            for(int j=0;j<matriz[i].length;j++)
            {
            System.out.print("Ingrese el elemento["+i+"]["+j+"]: ");
            matriz[i][j] = entrada.nextInt();
            }
        }
        for(int i=0;i<matriz.length;i++)
        {
            for(int j=0;j<matriz[i].length;j++)
            {
            System.out.print(matriz[i][j] + "\t");
            }
            System.out.println();
        }
        for(int i=0;i<matriz.length;i++)
        {
            for(int j=0;j<matriz[i].length;j++)
            {
                if(i!=j)
                {
                    suma = (suma + matriz[i][j]);
                }
            }
        }
        System.out.println("La suma es de: " + suma);
    }
}