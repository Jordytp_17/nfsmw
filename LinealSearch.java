public class LinealSearch {
    public static void main(String[] args) {
        int edades[] = {33,25,11,8,15,22,7,14,35,1,17,19,19,45};
        int pos = -1, value = 5;
        pos = linearSearch(edades, value);

        
        if(pos == -1)
        {
            System.out.format("El valor %d no se encuentra en la lista \n",value);
            return;
        }
        System.out.format("El valor %d se encuentra en la posicion %d en la lista \n",value,(pos+1));
    }
    public static int linearSearch(int array[], int value)
    {
        int pos = -1;
        for (int i=0; i<array.length; i++)
        {
            if(value == array[i])
            {
                pos = i;
                break;
            }
        }

        return pos;
    }
}