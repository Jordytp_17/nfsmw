import java.util.Arrays;
public class Metodos {
    public static void main(String[] args) {
        int vector [] = {3,5,7,9,11,13,15,17,19};
        int matrix[][] = {
            {4,5,6,7,8},
            {10,2,4,54,8},
            {2,5,8,9,6}
        };
        int sum = suma(vector);
        int sumaTabla = suma(matrix);
        System.out.println(Arrays.toString(vector));
        System.out.println("Suma: " + sum);
        printArray(vector);
        System.out.println("Suma: " + sum);
        printTable(matrix);
        System.out.println("Suma: " + sumaTabla);
    }
    public static void printArray(int array[]){
        System.out.println(Arrays.toString(array));

    }
    public static void printTable(int matrix[][]){
        for (int i=0;i<matrix.length;i++){
            printArray(matrix[i]);
        }
    }
    public static int suma(int array[]){
        int suma=0;
        for(int a : array){
            suma +=a;
        }
        return suma;
    }
    public static int suma(int matrix[][]){
        int suma=0;
        for(int i=0;i<matrix.length;i++)
        {
            /*for(int j=0;j<matrix[i].length;j++)
            {
                suma += matrix[i][j];
            }*/
            suma += suma(matrix[i]);
        }
        return suma;
    }
    public static float suma(float a,float b){
        return (a+b);
    }
}