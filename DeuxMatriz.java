import java.util.Scanner;
public class DeuxMatriz {
    public static void main(String[] args) {
        Scanner entrada;
        int fila=0, columna=0,fila2=0,columna2=0;
        entrada = new Scanner(System.in);

        System.out.println("Para poder multiplicar dos matrices el total de columnas de la primera matriz debe se igual al total de filas de la segunda matriz");
        System.out.println("Datos de la primer matriz");
        System.out.println("Ingrese el total de filas de la matriz #1:");
        fila = entrada.nextInt();
        System.out.println("Ingrese el total de columnas de la matriz#1");
        columna = entrada.nextInt();

        float matriz[][] = new float[fila][columna];

        System.out.println("\nIngrese los datos de la matriz #1\n");

        for(int i=0;i<matriz.length;i++)
        {
            for(int j=0;j<matriz[i].length;j++)
            {
            System.out.print("Ingrese el elemento["+i+"]["+j+"]: ");
            matriz[i][j] = entrada.nextFloat();
            }
        }

        System.out.println("\nDatos de la segunda matriz");
        System.out.println("Ingrese el total de filas de la matriz #2:");
        fila2 = entrada.nextInt();
        System.out.println("Ingrese el total de columnas de la matriz#2");
        columna2 = entrada.nextInt();
        
        float matrix[][] = new float[fila2][columna2];
        
        System.out.println("\nIngrese los datos de la matriz #2\n");
        for(int i=0;i<matrix.length;i++)
        {
            for(int j=0;j<matrix[i].length;j++)
            {
            System.out.print("Ingrese el elemento["+i+"]["+j+"]: ");
            matrix[i][j] = entrada.nextFloat();
            }
        }
        
        System.out.println("\nMatriz #1\n");
        for(int i=0;i<matriz.length;i++)
        {
            for(int j=0;j<matriz[i].length;j++)
            {
            System.out.print(matriz[i][j] + "\t");
            }
            System.out.println();
        }

        System.out.println("\nMatriz #2\n");        
        for(int i=0;i<matrix.length;i++)
        {
            for(int j=0;j<matrix[i].length;j++)
            {
            System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }

        System.out.println("\n");
        if(columna!=fila2)
        {
            System.out.println("NO SE PUEDE MULTIPLICAR LA MATRIZ");
            System.out.format("Las columnas de la matriz #1 son: %d y las filas de la matriz #2 son: %d",columna,fila2);
            System.out.println("Las columnas son diferentes a las filas");
        }
        else
        {
            System.out.println("Calculando la multiplicacion");
        }
    }
}