/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

import ni.edu.uni.programacion1.pojo.Empleado;

/**
 *
 * @author Sistemas26
 */
public class VisualizarEmpleadoPorMunicipio {
    public static void printEmpleado(Empleado e){
        System.out.println("Codigo: " + e.getCodigo());
        System.out.println("Nombre: " + e.getPrimerNombre());
        System.out.println("Apellido: " + e.getPrimerApellido());
        System.out.println("Direccion: " + e.getDireccion());
        System.out.println("Telefono: " + e.getTelefono());
        System.out.println("Celular: " + e.getCelular());
        System.out.println("Cedula: " + e.getCedula());
        System.out.println("Sexo: " + e.getSexo());
        System.out.println("Nivel academico: " + e.getNivelAcademico());
        System.out.println("Municipio: " + e.getMunicipio());
    }
}
