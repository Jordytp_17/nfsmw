/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import ni.edu.uni.programacion1.data.DepartamentoData;
import ni.edu.uni.programacion1.data.EmpleadoData;
import ni.edu.uni.programacion1.data.MunicipioData;
import ni.edu.uni.programacion1.enums.NivelAcademico;
import ni.edu.uni.programacion1.enums.Sexo;
import ni.edu.uni.programacion1.pojo.Departamento;
import ni.edu.uni.programacion1.pojo.Empleado;
import ni.edu.uni.programacion1.pojo.Municipio;

/**
 *
 * @author DocenteFCyS
 */
public class ConsoleGestionEmpleado {

    public static void agregarEmpleado(BufferedReader reader, EmpleadoData edata) throws IOException {
        DepartamentoData dData = new DepartamentoData();
        MunicipioData mData = new MunicipioData();

        int codigo = ConsoleReader.readInt(reader, "Codigo: ");
        String cedula = ConsoleReader.readCedula(reader, "Cedula: ");
        String name1 = ConsoleReader.readLetter(reader, "Primer Nombre: ");
        String name2 = ConsoleReader.readLetter(reader, "Segundo Nombre: ");
        String lastname1 = ConsoleReader.readLetter(reader, "Primer Apellido: ");
        String lastname2 = ConsoleReader.readLetter(reader, "Segundo Apellido: ");
        String address = ConsoleReader.readString(reader, "Direccion: ");
        String phoneNumber = ConsoleReader.readTelefono(reader, "Telefono: ");
        String mobileNumber = ConsoleReader.readTelefono(reader, "Celular: ");
        
        int sexoId;
        do {
            sexoId = ConsoleReader.readInt(reader, 
                    "1. Femenino.\n2. Masculino.\nOpcion: ");
        } while (sexoId <= 0 || sexoId > 2);
        Sexo sexo = Sexo.values()[sexoId - 1];
        
        int nivelId;
        do {
            nivelId = ConsoleReader.readInt(reader,
                    "1. Primaria.\n2. Tecnico Medio.\n3. Bachiller.\n"
                    + "4. Tecnico Superior.\n5. Licenciado.\n"
                    + "6. Ingeniero.\n7.PostGrado.\n"
                    + "8. Especialista.\n9. Maestria\n"
                    + "10. Doctorado.\n11.Post Doctorado.\n"
                    + "Opcion: ");
        } while (nivelId <= 0 || nivelId > 11);
        NivelAcademico na = NivelAcademico.values()[nivelId - 1];
       
        Departamento[] departamentos = dData.getDepartamento();
        String[] nombresDepartamentos = new String[17];
        int i = 0;
        for(Departamento d : departamentos){
            nombresDepartamentos[i++] = d.getNombre();
        }
        int dId;
        do {
            dId = ConsoleReader.readInt(reader, "Digite entre 1-17 "
                    + "para seleccionar un departamento\n "
                    + Arrays.toString(nombresDepartamentos));
        }while(dId <= 0 || dId > 17);
        Departamento d = dData.getDepartamentoById(dId);
        Municipio[] municipios = mData.getMunicipioByDepartamento(d);
        for(Municipio m : municipios){
            
        }
        
        Empleado e = new Empleado(codigo, cedula, phoneNumber, phoneNumber, phoneNumber, lastname2, address, name2, cedula, sexo, na,municipios[0]);
        edata.add(e);
    }

    public static void editarEmpleado(BufferedReader reader, EmpleadoData edata) throws IOException {
        int codeId;
        do{
            codeId = ConsoleReader.readInt(reader,"");
            
        }while(codeId!=0);
    }

    public static void eliminarEmpleado() {

    }
}
