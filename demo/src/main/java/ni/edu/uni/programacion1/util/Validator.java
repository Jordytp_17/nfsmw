/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author DocenteFCyS
 */
public class Validator {
    
    public static boolean validate(String pattern, String text){
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        return m.matches();
    }
    
    public static boolean isInt(String text){
        return validate("\\d+",text);
    }
    
    public static boolean isLetter(String text){
        return validate("(?i)(^[a-z])((?![ .,'-]$)[a-z .,'-]){0,24}$",text);
    }
    
    public static boolean isCedula(String text){
        return validate("",text);
    }
    
    public static boolean isPhoneNumber(String text){
        return validate("",text);
    }
    public static boolean isCodeID(String text){
        return validate("",text);
    }    
}
