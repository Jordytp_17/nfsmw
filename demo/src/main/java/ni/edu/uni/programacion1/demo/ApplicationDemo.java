/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.demo;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ni.edu.uni.programacion1.console.ConsoleDemo;
import ni.edu.uni.programacion1.pojo.Empleado;

/**
 *
 * @author DocenteFCyS
 */
public class ApplicationDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try {
            ConsoleDemo.start();
        } catch (IOException ex) {
            Logger.getLogger(ApplicationDemo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void printEmpleado(Empleado e){
        System.out.println("Codigo: " + e.getCodigo());
        System.out.println("Nombre: " + e.getPrimerNombre());
        System.out.println("Apellido: " + e.getPrimerApellido());
        System.out.println("Direccion: " + e.getDireccion());
        System.out.println("Telefono: " + e.getTelefono());
        System.out.println("Celular: " + e.getCelular());
    }
    
}
