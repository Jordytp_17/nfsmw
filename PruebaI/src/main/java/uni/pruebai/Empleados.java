/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.pruebai;

/**
 *
 * @author Sistemas13
 */
public class Empleados {
    private int Id;
    private String Carnet;
    private String Nombre;
    private String Apellido;
    private String Cedula;
    private String Direccion;
    private String Telefono;
    private int SalarioHora;

    public Empleados() {
    }

    public Empleados(int Id, String Carnet, String Nombre, String Apellido, String Cedula, String Direccion, String Telefono, int SalarioHora) {
        this.Id = Id;
        this.Carnet = Carnet;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Cedula = Cedula;
        this.Direccion = Direccion;
        this.Telefono = Telefono;
        this.SalarioHora = SalarioHora;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getCarnet() {
        return Carnet;
    }

    public void setCarnet(String Carnet) {
        this.Carnet = Carnet;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public int getSalarioHora() {
        return SalarioHora;
    }

    public void setSalarioHora(int SalarioHora) {
        this.SalarioHora = SalarioHora;
    }
    
    
    
}
