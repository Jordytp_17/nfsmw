public class PrimerEjercicio {
    public static void main(String[] args) {

        // Declarando variables

        int A;
        int B;
        int C;
        int D;
        int K;

        // Asignando valores
        
        A = 13;
        B = 11;
        C = 17;
        D = 19;
        

        System.out.println("A: " + A);
        System.out.println("B: " + B);
        System.out.println("C: " + C);
        System.out.println("D: " + D);
        System.out.println("<<*********************>>");

        K = B;
        B = C;
        C = A;
        A = D;
        D = K;

        System.out.println("A: " + A);
        System.out.println("B: " + B);
        System.out.println("C: " + C);
        System.out.println("D: " + D);
        
    }
}