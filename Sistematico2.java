import java.util.Scanner;
public class Sistematico2 {
    public static void main(String[] args) {
        Scanner entrada;
        float carrera[][] = new float[4][3];
        float suma[] = new float [4];
        int orden[] = new int [4];
        float Conductor1 = 0,Conductor2 = 0,Conductor3 = 0,Conductor4 = 0,add=0;
        entrada = new Scanner(System.in);
        System.out.println("Ingrese los resultados de la carrera");
        for(int i=0;i<carrera.length;i++)
        {
            System.out.format("Conductor #%d: \n",(i+1));
            for(int j=0;j<carrera[i].length;j++)
            {
            System.out.print("Ingrese el tiempo["+i+"]["+j+"]: ");
            carrera[i][j] = entrada.nextFloat();
            }
        }
        for(int i=0;i<carrera.length;i++)
        {
            for(int j=0;j<carrera[i].length;j++)
            {
            System.out.print(carrera[i][j] + "\t");
            }
            System.out.println();
        }
        for(int i=0;i<carrera.length;i++)
        {
            add = 0;
            for(int j=0;j<carrera[i].length;j++)
            {
                add = add + carrera[i][j];
            }
            suma [i] = ((add/3)*60);
        }
        for(int i=0; i<suma.length;i++)
        {
            System.out.format("Conductor #%d, promedio en segundos %f  \n",(i+1),suma[i]);
        }
        //Ordenando
        System.out.println("Promedio sin ordenar: ");
        System.out.println(java.util.Arrays.toString(suma));
        System.out.println("==================================");
        insertionSortImperative(suma);
        System.out.println("Arreglo ordenado: ");
        System.out.println("==================================");
        System.out.println(java.util.Arrays.toString(suma));
        System.out.println("\n\n");
        System.out.println("Llegada");

        //Imprimiendo tabla
        System.out.println("Tabla en segungos");
        System.out.format("%20s %20s\n","Tiempo","Diferencia");
        for(int i=0; i<suma.length;i++)
        {
            System.out.format("           %f           %f \n",suma[i],(suma[i]-suma[0]));
        }
    }
    public static void insertionSortImperative(float[] input) {
        for (int i = 1; i < input.length; i++) { 
            float key = input[i]; 
            int j = i - 1;
            while (j >= 0 && input[j] > key) {
                input[j + 1] = input[j];
                j = j - 1;
            }
            input[j + 1] = key; 
        }
    }

}