/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persona;

/**
 *
 * @author Sistemas36
 */
public class Empleado extends Persona{
    private float salario;

    public Empleado() {
    }

    public Empleado(String nombre, int edad) {
        super(nombre, edad);
    }

    public Empleado(float salario) {
        this.salario = salario;
    }

    public Empleado(float salario, String nombre, int edad) {
        super(nombre, edad);
        this.salario = salario;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }


}
