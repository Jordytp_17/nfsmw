/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persona;

import java.util.Scanner;

/**
 *
 * @author Sistemas36
 */
public class Implementar {
    public static void main(String[] args){
        Scanner scan =  new Scanner(System.in);
        Empleado e = new Empleado();
        
        System.out.println("Ingrese el nombre: ");
        e.setNombre(scan.next());
        System.out.println("Ingrese la edad: ");
        e.setEdad(scan.nextInt());
        System.out.println("Ingrese el salario: ");
        e.setSalario(scan.nextFloat());
        
        
        System.out.println("Datos: ");
        System.out.println("Nombre: " + e.getNombre());
        System.out.println("Edad: " + e.getEdad());
        System.out.println("Salario: " + e.getSalario());
        System.out.println("Antiguedad 15%(10 años: " + e.getSalario()*0.15);
        System.out.println("Inss laboral 7%: " + (e.getSalario()+e.getSalario()*0.15)*0.7);
        System.out.println("Inss patronal 22%: " + (e.getSalario()+e.getSalario()*0.15)*0.22);
        System.out.println("Inatec: "+ (e.getSalario()+e.getSalario()*0.15)*0.2);
        System.out.println("Vacaciones: "+ (e.getSalario()+e.getSalario()*0.15)/12);
        System.out.println("Treceavo mes: "+ (e.getSalario()+e.getSalario()*0.15)/12);
    }
}
